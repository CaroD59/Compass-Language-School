import styled from 'styled-components';

const ContactCSS = styled.header`
  .App {
    font-family: sans-serif;
    text-align: center;
  }
  .messages {
    margin-top: 30px;
    display: flex;
    flex-direction: column;
  }
  .message {
    border-radius: 20px;
    padding: 8px 15px;
    margin-top: 5px;
    margin-bottom: 5px;
    list-style: none;
    text-align: left;
    margin-right: 25%;
    background-color: #eee;
    position: relative;
  }

  .message:last-child::before {
    content: '';
    position: absolute;
    z-index: 0;
    bottom: 0;
    left: -7px;
    height: 20px;
    width: 20px;
    background: #eee;
    border-bottom-right-radius: 15px;
  }
  .message:last-child::after {
    content: '';
    position: absolute;
    z-index: 1;
    bottom: 0;
    left: -10px;
    width: 10px;
    height: 20px;
    background: white;
    border-bottom-right-radius: 10px;
  }
`;

export default ContactCSS;
