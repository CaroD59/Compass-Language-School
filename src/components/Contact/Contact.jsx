import ContactCSS from './ContactCSS';
import { useState } from 'react';

export default function App() {
  const [messages, setMessages] = useState(['Hello There!']);
  const [userInput, setuserInput] = useState('');

  const handleSubmit = (event) => {
    event.preventDefault();
    setuserInput('');
    alert('Your message has been sent !');
    setMessages([...messages, userInput]);
  };

  return (
    <ContactCSS>
      <div className='App'>
        <h1>Welcome to your chat Application</h1>
        <ul className='messages'>
          {messages.map((message, index) => (
            <li key={index} className='message'>
              {message}
            </li>
          ))}
        </ul>
        <div>
          <form onSubmit={handleSubmit} className='chat-box'>
            <input
              type='text'
              value={userInput}
              onChange={(event) => setuserInput(event.target.value)}
            />
            <button>Send Message</button>
          </form>
        </div>
      </div>
    </ContactCSS>
  );
}
