import Contact from './Contact';
import { BrowserRouter } from 'react-router-dom';

export default function MainPageContact() {
  return (
    <BrowserRouter>
      <Contact />
    </BrowserRouter>
  );
}
