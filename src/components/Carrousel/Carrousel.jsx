import React from 'react';
import { Slide } from 'react-slideshow-image';
import 'react-slideshow-image/dist/styles.css';
import SCarrousel from './CarrouselCSS';

const properties = {
  duration: 3500,
  transitionDuration: 500,
  infinite: true,
  indicators: true,
  arrow: true,
};

export default function Carrousel({ theme }) {
  return (
    <SCarrousel className='container' theme={theme}>
      <Slide {...properties}>
        <div className='slide'>
          <div>
            <img src='/Images/Carrousel/slide1.png' alt='img1' />
          </div>
        </div>
        <div className='slide'>
          <div>
            <img src='/Images/Carrousel/slide2.png' alt='img1' />
          </div>
        </div>
      </Slide>
    </SCarrousel>
  );
}
