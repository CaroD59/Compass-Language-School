import styled from 'styled-components';

const NavBarCSS = styled.header`
  /* text-align: center; */
  font-size: 1.2em;
  height: 350px;
  background: linear-gradient(#f0f03d, white);
  font-family: 'Arial';

  .BigContainer {
    display: flex;
    justify-content: space-around;
  }

  .LogoCLS {
    width: auto;
    height: 250px;
  }
  .bloc1 {
    background-color: rgb(0, 66, 77);
    color: yellow;
    text-align: center;
    padding-top: 1em;
    height: 50px;
    width: 250px;
    border-bottom-left-radius: 20px;
    border-bottom-right-radius: 20px;
    box-shadow: 1px 8px 15px 2px rgba(0, 52, 61, 0.84);
  }
  .NavBar {
    display: flex;
    flex-direction: row;
    justify-content: space-around;
    padding-top: 20px;
    position: relative;
  }

  .NavBar li {
    list-style: none;
    margin-left: 15px;
    margin-right: 15px;
  }

  .NavBar li a {
    color: #565656;
    text-decoration: none;
  }

  .NavBar li a:hover {
    color: black;
    font-weight: bold;
  }

  .btn {
    display: none;
  }

  @media screen and (max-width: 500px) {
    .NavBar {
      flex-direction: column;
      text-align: center;
      height: 200px;
    }

    .items:nth-child(1) {
      border-top: 1px solid #565656;
      margin-top: 50px;
    }
    .items {
      height: 50px;
      width: 100px;
      border-bottom: 1px solid #565656;
      line-height: 50px;
    }
    .btn {
      display: block;
      position: absolute;
      right: 10px;
      top: 10px;
      color: #333;
    }
    .bloc1 {
      display: none;
    }
    .LogoCLS {
      width: 150px;
      height: 120px;
    }
  }
`;

export default NavBarCSS;
