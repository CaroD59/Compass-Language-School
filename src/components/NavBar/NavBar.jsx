import { Switch, Route, NavLink } from 'react-router-dom';
import { useState, useEffect } from 'react';
import MainPage from '../MainPage/MainePage';
import CoursetFormules from '../CoursetFormules/CoursetFormules';
import Recrutement from '../Recrutement/Recrutement';
import Blog from '../Blog/Blog';
import Contact from '../Contact/MainPageContact.jsx';
import NavBarCSS from './NavBarCSS';

export default function NavBar() {
  const [toggleMenu, setToggleMenu] = useState(false);
  const [largeur, setLargeur] = useState(window.innerWidth);
  const toggleNavSmallScreen = () => {
    setToggleMenu(!toggleMenu);
  };

  useEffect(() => {
    const changeWidth = () => {
      setLargeur(window.innerWidth);
      if (window.innerWidth > 500) {
        setToggleMenu(false);
      }
    };
    window.addEventListener('resize', changeWidth);
    return () => {
      window.removeEventListener('resize', changeWidth);
    };
  }, []);
  return (
    <NavBarCSS>
      <header>
        <div className='BigContainer'>
          <div className='bloc1'>Cours de langues en ligne</div>{' '}
          <div>
            <img
              src='Images/Icônes/BigLogoCLS.png'
              alt=''
              className='LogoCLS'
            />
          </div>
        </div>
        {(toggleMenu || largeur > 500) && (
          <ul className='NavBar'>
            <li className='items'>
              <NavLink exact to='/'>
                Accueil
              </NavLink>
            </li>
            <li className='items'>
              <NavLink to='/CoursetFormules'>Cours et formules</NavLink>
            </li>
            <li className='items'>
              <NavLink to='/Recrutement'>Recrutement</NavLink>
            </li>
            <li className='items'>
              <NavLink to='/Blog'>Blog</NavLink>
            </li>
            <li className='items'>
              <NavLink to='/Contact'>Nous contacter</NavLink>
            </li>
          </ul>
        )}
        <button onClick={toggleNavSmallScreen} className='btn'>
          BTN
        </button>
      </header>
      <main>
        <Switch>
          <Route exact path='/' component={MainPage} />
          <Route path='/CoursetFormules' component={CoursetFormules} />
          <Route path='/Recrutement' component={Recrutement} />
          <Route path='/Blog' component={Blog} />
          <Route path='/Contact' component={Contact} />
        </Switch>
      </main>
    </NavBarCSS>
  );
}
